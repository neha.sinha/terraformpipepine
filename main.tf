provider "aws" {
  region                  = "us-east-1"
  shared_credentials_file = /home/ec2-user/.aws/credentials"
  
}

resource "aws_instance" "bastionAssignment4" {
  ami           = var.instanceami
  instance_type = var.instancetype
  key_name = var.keyname
  subnet_id = var.subnetid
  associate_public_ip_address = true
  vpc_security_group_ids = [aws_security_group.bastionAssignment4.id]
  tags = {
    Name = var.bastioninstancename
  }
}
resource "aws_instance" "sonarqubeAssignment4" {
  ami           = var.instanceami
  instance_type = var.instancetype
  key_name = var.keyname
  subnet_id = var.subnetid
  vpc_security_group_ids = [aws_security_group.sonarqubeAssignment4.id]
  tags = {
    Name = var.softwareinstancename
  }
}
resource "aws_security_group" "bastionAssignment4" {
  name        = var.bastionsgname 
  vpc_id      = var.vpcid

  ingress  {
   
    from_port   = var.ingress_fromport
    to_port     = var.ingress_toport
    protocol    = var.ingress_protocal
    cidr_blocks = ["0.0.0.0/0"]
  }
    egress {
    
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    
  }
  tags = {
    "Name" = var.bastionsgname
  }

}
resource "aws_security_group" "sonarqubeAssignment4" {
  name        = var.softwaresgname  
  vpc_id      = var.vpcid

  ingress  {
    
    from_port   = var.ingress_fromport
    to_port     = var.ingress_toport
    protocol    = var.ingress_protocal
    cidr_blocks = ["0.0.0.0/0"] 
  }
    egress {
    
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    
  }
  tags = {
    "Name" = var.softwaresgname
  }

}
resource "aws_key_pair" "access_key" {
  key_name   = var.keyname
  public_key = var.publickey
}
output sgid1 {
value=aws_security_group.sonarqubeAssignment4.id
}
output sgid2 {
value=aws_security_group.bastionAssignment4.id
}
