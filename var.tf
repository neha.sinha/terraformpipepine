variable "vpcid" {
type =string
default ="vpc-e07a129d"
}



variable "subnetid" {
type =string  
default = "subnet-20d8817f"
}
variable "instanceami" {
type =string 
default ="ami-0747bdcabd34c712a"   
}
variable "instancetype" {
type =string 
default = "t2.micro"   
}
variable "bastioninstancename" {
type =string 
default = "bastion"

}
variable "softwareinstancename" {
type =string 
defalt = "sonarqube"

}


variable "bastionsgname" {
    type = string
    default = "bastionAssignment4gp"
}


variable "softwaresgname" {
    type = string
    default = "sonarqubeAssignment4gp"
}




variable "ingress_fromport" {
    type = number
    default = 22
}


variable "ingress_toport" {
    type = number
    default = 22
}

variable "ingress_protocal"{
    type = string
    default = "tcp"
}
variable "keyname" {
type = string
default="mumbaiubuntu"
}


